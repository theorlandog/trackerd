/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package trackerd

import (
	"context"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	pb "gitlab.com/skyhuborg/proto-trackerd-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"log"
	"net"
	"os"
)

type Status struct {
	Id    int    `bson:"_id,omitempty"`
	Uuid  string `bson:"uuid"`
	Name  string `bson:"name"`
	Build string `bson:"build"`
}

type Server struct {
	Handle     *grpc.Server
	ListenPort int
	EnableTls  bool
	TlsKey     string
	TlsCert    string
	DbHost     string
	DbPort     int
	DbUser     string
	DbPassword string
	DbName     string

	dbUri string
	db    *gorm.DB
}

func (s *Server) ConnectDb() *gorm.DB {
	db, err := gorm.Open("mysql", s.dbUri)

	if err != nil {
		log.Println(err)
		return nil
	}
	return db
}

func (s *Server) Start() {
	s.dbUri = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", s.DbUser, s.DbPassword, s.DbHost, s.DbPort, s.DbName)

	s.db = s.ConnectDb()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.ListenPort))

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s.Handle = grpc.NewServer()

	pb.RegisterTrackerdServer(s.Handle, s)

	grpclog.SetLogger(log.New(os.Stdout, "trackerd: ", log.LstdFlags))

	grpclog.Printf("Starting GRPC on port %d", s.ListenPort)

	if err := s.Handle.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}

func (s *Server) AddSensor(ctx context.Context, in *pb.SensorReport) (*pb.SensorResponse, error) {
	grpclog.Printf("Report received\n")

	/*
			col := c.Database("skyhub").Collection("sensor")

			_, err := col.InsertOne(context.TODO(), in)

		    if err != nil {
		        log.Fatal(err)
		    }
	*/

	return &pb.SensorResponse{}, nil
}
